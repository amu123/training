package za.co.Infowarestudios.support.call.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by victorrikhotso on 2016/02/29.
 */
@Service
public class SecurityService {

	private final Logger logger = LoggerFactory.getLogger(getClass().getName());

	private final static String RESOURCE_ID = "wallet_balances_api";

	/**
	 *
	 * @param authentication
	 * @param authorizeDetails
     */

	public void authorizeRequest(OAuth2Authentication authentication, HashMap<String,String> authorizeDetails) {

		CustomAuthentication customAuthentication = extractAccessTokenProperties(authentication);
		// TODO: Disbaled check for ussd client token
		// TODO: Will need to be fixed with an internal access token between the services (TRUST SERVICE)
		if(!customAuthentication.getClientId().equalsIgnoreCase("ussd_client")){
			// Authorize resourceId:
			customAuthentication.getResources().forEach( p -> { logger.info("resource : "+ p);});
			List<String> matchedResources = customAuthentication.getResources().stream()
					.filter(RESOURCE_ID::equalsIgnoreCase)
					.collect(Collectors.toList());
			if(matchedResources.size() == 0){
				throw new AuthorizeRequestException("Invalid resource.");
			}

			// Authorize scope:
			if(authorizeDetails.containsKey("scopes")){
				String scope = authorizeDetails.get("scopes");

				boolean status = false;

				for(String hh : customAuthentication.getScopes()) {
					if(scope.equalsIgnoreCase(hh)) status = true;
				}

				if(!status){
					throw new AuthorizeRequestException("Invalid scope.");
				}
			}

			// Authorize authority:
			if(authorizeDetails.containsKey("authorities")){
				Set<String> authorities = new HashSet<String>(Arrays.asList(authorizeDetails.get("authorities").toLowerCase().split(",")));
				List<String> macthedAuthorities = customAuthentication.getAuthorities().stream()
						.filter(authorities::contains)
						.collect(Collectors.toList());
				if(macthedAuthorities.size() == 0){
					throw new AuthorizeRequestException("Invalid authority.");
				}
			}

			// Authorize userid:
			if(authorizeDetails.containsKey("userId")){
				if(!customAuthentication.getUserId().equalsIgnoreCase(authorizeDetails.get("userId"))){
					throw new AuthorizeRequestException("Invalid userId.");
				}
			}

		}

	}


	private CustomAuthentication extractAccessTokenProperties(OAuth2Authentication authentication){
		CustomAuthentication customAuthentication = new CustomAuthentication();

		String oauthentication = authentication.getUserAuthentication().getDetails().toString();

		logger.error("--- " + oauthentication);

		// Get the userId:
		customAuthentication.setUserId(authentication.getName());

		// Get the clientId:
		customAuthentication.setClientId(stripSingleValue(oauthentication,"clientId","=",","));
		logger.info(customAuthentication.getClientId());

		// Get the scopes:
		customAuthentication.setScopes(stripMultipleValues(oauthentication,"scope","=","]"));
		logger.info(customAuthentication.getScopes().toString());

		// isUser
		String isClient = stripSingleValue(oauthentication,"clientOnly","=",",");
		if(isClient.equalsIgnoreCase("false")){
			customAuthentication.setUser(true);
			customAuthentication.setClient(false);
		}
		else if(isClient.equalsIgnoreCase("true")){
			customAuthentication.setUser(false);
			customAuthentication.setClient(true);
		}

		// resourceIds
		customAuthentication.setResources(stripMultipleValues(oauthentication,"resourceIds","=","],"));
		logger.info(customAuthentication.getResources().toString());

		// authorities
		customAuthentication.setAuthorities(stripAuthorities(oauthentication,"authority","=","}"));
		logger.info(customAuthentication.getAuthorities().toString());

		return customAuthentication;
	}

	public String stripSingleValue(String origin,String key,String seperator, String endString){
		int fromIndex = origin.indexOf(key);
		int signIndex = origin.indexOf(seperator,fromIndex);
		int endIndex = origin.indexOf(endString,signIndex);
		String singleValue = origin.substring(signIndex+1,endIndex).trim();
		singleValue = singleValue.replace(", ","").replace("[","").replace("]","");
		return singleValue;
	}

	public HashSet<String> stripMultipleValues(String origin,String key,String seperator, String endString){
		int fromIndex = origin.indexOf(key);
		int signIndex = origin.indexOf(seperator,fromIndex);
		int endIndex = origin.indexOf(endString,signIndex);
		String multipleValues = origin.substring(signIndex+1,endIndex+1).trim();
		multipleValues = multipleValues.replace(", ",",").replace("[","").replace("]","");
		return new HashSet<>(Arrays.asList(multipleValues.toLowerCase().split(",")));
	}

	public HashSet<String> stripAuthorities(String origin,String key,String seperator, String endString){
		int fromIndex = origin.indexOf(key);
		int signIndex = origin.indexOf(seperator,fromIndex);
		int endIndex = origin.indexOf(endString,signIndex);
		String multipleValues = origin.substring(signIndex+1,endIndex).trim();
		multipleValues = multipleValues.replace(", ",",").replace("[","").replace("]","");
		return new HashSet<>(Arrays.asList(multipleValues.toLowerCase().split(",")));
	}

}
