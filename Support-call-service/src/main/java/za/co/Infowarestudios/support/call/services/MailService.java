package za.co.Infowarestudios.support.call.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import za.co.Infowarestudios.support.call.SendSupportMailMessage;
import za.co.Infowarestudios.support.call.entity.Support;
import za.co.Infowarestudios.support.call.repository.SupportRepository;

import javax.mail.MessagingException;
import java.text.SimpleDateFormat;

/**
 * Created by seanmatabire on 2016/04/12.
 */
@Component
public class MailService {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    SupportRepository supportRepository;

    @Autowired
    SendSupportMailMessage sendSupportMailMessage;

    @Scheduled(cron = "0 * * * * *" )
    public void mailingService(){
        //Constructor
        try {
            // Your task process
            processUnsentMails();
            System.out.println(" email delivery process executed");

        } catch (Exception ex) {

            System.out.println("error running delivery check: " + ex.getMessage());
        }
    }

    public void processUnsentMails() throws MessagingException {
        Iterable iterable = supportRepository.findByDelivered(false);
        if(iterable != null) {
            for (Object o : iterable) {
                Support support = (Support) o;
                sendSupportMailMessage.supportMailMessage(support);
            }
        }
        else { System.out.println("\n\n\n\nNo unsent emails\n\n\n\n");}
    }
}
