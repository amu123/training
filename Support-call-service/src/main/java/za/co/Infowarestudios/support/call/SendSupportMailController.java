package za.co.Infowarestudios.support.call;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;
import za.co.Infowarestudios.support.call.entity.Support;
import za.co.Infowarestudios.support.call.security.SecurityService;
import za.co.Infowarestudios.support.call.services.SupportServices;

import java.util.HashMap;

/**
 * Created by Sean Matabire on 3/23/2016.
 */
@RestController
public class SendSupportMailController {
    private static final Logger logger = LoggerFactory.getLogger(SendSupportMailController.class);

    @Autowired
    private SecurityService securityService;

@Autowired
private SupportServices supportServices;

    @RequestMapping(value = "/mail", method = RequestMethod.POST)
    public ResponseEntity sendSupportEmail(@RequestBody String json,
                                           @RequestHeader(value="Authorization") String authorizationHeader,
                                           OAuth2Authentication currentUser) {
        HashMap<String, String> authorizeDetails = new HashMap<>();
        authorizeDetails.put("scopes", "read");
        authorizeDetails.put("authorities", "TRUSTED_MOBILE,TRUSTED_CLIENT");
        logger.info(json);
        securityService.authorizeRequest(currentUser, authorizeDetails);

        try
        {
            ObjectMapper mapper = new ObjectMapper();
            final JsonNode data = mapper.readTree(json);
            logger.info(json);

            String email = data.findValue("email").asText();
            String subject = data.findValue("supportType").asText();
            String bodyText = data.findValue("supportDescription").asText();

            Support supportMessage = new Support(email,subject,bodyText,false);
            supportServices.addSupport(supportMessage);
            return new ResponseEntity<>(HttpStatus.OK);

        }
        catch(Exception error)
        {
            error.printStackTrace();
            return new ResponseEntity<>(error.getLocalizedMessage(),HttpStatus.BAD_REQUEST);
        }

    }

}
