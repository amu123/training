package za.co.Infowarestudios.support.call.security;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by victorrikhotso on 2016/03/04.
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class AuthorizeRequestException extends RuntimeException {

  public AuthorizeRequestException(String message) {
    super(message);
  }
}
