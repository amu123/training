package za.co.Infowarestudios.support.call;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.Infowarestudios.support.call.entity.Support;
import za.co.Infowarestudios.support.call.repository.SupportRepository;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

/**
 * Created by Sean Matabire on 3/23/2016.
 */
@Component
public class SendSupportMailMessage {

    @Autowired
    SupportRepository supportRepository;

    public void supportMailMessage(Support support) throws MessagingException{

        // Sender's email ID needs to be mentioned
        final String username = "selpaltest@gmail.com";//change accordingly
        final String password = "test.selpal";//change accordingly

        // Assuming you are sending email through relay.jangosmtp.net
        String host = "smtp.gmail.com";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        System.out.println("In the sender....");
        // Get the Session object.
        Session session = Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        Message message = new MimeMessage(session);
        // Send message
        try {
            Transport.send(buildMessage(support,message));
            support.setDelivered(true);

            System.out.println("Sent message successfully....\n\n\n"+ support.toString() );
            supportRepository.save(support);
            System.out.println("Sent message successfully....");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Message buildMessage(Support support,Message message){

        // Recipient's email ID needs to be mentioned.
        String to = "selpal@infowarestudios.co.za";//change accordingly

        try {
            // Create a default MimeMessage object.

            // Set From: header field of the header.
            message.setFrom(new InternetAddress("support@selpal.co.za"));

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.addRecipient(Message.RecipientType.CC, new InternetAddress("selpaluat@infowarestudios.co.za"));
            message.addRecipient(Message.RecipientType.CC, new InternetAddress("info@selpal.co.za"));
            // Set Subject: header field
            message.setSubject(support.getSubject());

            String body = support.getBody(); // here email body
            // Now set the actual message
            message.setText(body);

            message.setSentDate(new Date());

            return message;

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }


}
