package za.co.Infowarestudios.support.call;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.slf4j.Logger;


import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Tshepo on 3/23/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SupportCallServiceApplication.class)
@WebAppConfiguration
public class SendSupportMailControllerTest {
    private MockMvc mockMvc;

    private MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), org.springframework.http.MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private SendSupportMailMessage sendSupportMailMessage;

    @Autowired
    private SendSupportMailController sendSupportMailController;

    @Before
    public void setUpEmail(){
        assertNotNull(webApplicationContext);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    @Test
    public void createSupportLog(){
        String json = "{\"userID\": \"tle0001\"," +
                "\"supportType\" : \"cannot  view products\", " +
                "\"supportDescription\":\"My company details has changed and I need details to be updated\" }";
        try{
            this.mockMvc.perform(put("/support")
                    .contentType(APPLICATION_JSON_UTF8)
                    .content(json))
                    .andExpect(status().isOk());
        }catch (Exception e){
            fail(e.getLocalizedMessage());
        }
    }
}
