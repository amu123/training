package za.co.Infowarestudios.support.call.security;

import org.springframework.stereotype.Component;

import java.util.HashSet;

/**
 * Created by victorrikhotso on 2016/03/03.
 */
@Component
public class CustomAuthentication {

    private String userId;
    private String clientId;
    private HashSet<String> authorities;
    private HashSet<String> scopes;
    private HashSet<String> resources;
    private boolean isClient;
    private boolean isUser;

    public CustomAuthentication() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public HashSet<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(HashSet<String> authorities) {
        this.authorities = authorities;
    }

    public HashSet<String> getScopes() {
        return scopes;
    }

    public void setScopes(HashSet<String> scopes) {
        this.scopes = scopes;
    }

    public HashSet<String> getResources() {
        return resources;
    }

    public void setResources(HashSet<String> resources) {
        this.resources = resources;
    }

    public boolean isClient() {
        return isClient;
    }

    public void setClient(boolean client) {
        isClient = client;
    }

    public boolean isUser() {
        return isUser;
    }

    public void setUser(boolean user) {
        isUser = user;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "CustomAuthentication{" +
                "userId='" + userId + '\'' +
                ", clientId='" + clientId + '\'' +
                ", authorities=" + authorities +
                ", scopes=" + scopes +
                ", resources=" + resources +
                ", isClient=" + isClient +
                ", isUser=" + isUser +
                '}';
    }

    public CustomAuthentication(String userId, String clientId,
                                HashSet<String> authorities,
                                HashSet<String> scopes,
                                HashSet<String> resources,
                                boolean isClient,
                                boolean isUser) {
        this.userId = userId;
        this.clientId = clientId;
        this.authorities = authorities;
        this.scopes = scopes;
        this.resources = resources;
        this.isClient = isClient;
        this.isUser = isUser;
    }
}
