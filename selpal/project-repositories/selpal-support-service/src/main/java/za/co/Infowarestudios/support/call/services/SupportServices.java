package za.co.Infowarestudios.support.call.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.Infowarestudios.support.call.entity.Support;
import za.co.Infowarestudios.support.call.repository.SupportRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by seanmatabire on 2016/04/12.
 */
@Component
public class SupportServices {

    @Autowired
    SupportRepository supportRepository;

    public Support addSupport(Support support){

        return supportRepository.save(support);
    }

    public List<Support> getSupports(String registrarId){
        Iterable iterable = supportRepository.findAll();
        List<Support> supportcalls = new ArrayList<>();

        if(iterable != null){
            for(Object o : iterable) {
                Support support = (Support)o;
                supportcalls.add(support);
            }

            return supportcalls;
        }
        else throw new NullPointerException(registrarId + " has no operators");
    }

    public List<Support> getUnsentSupports(){
        Iterable iterable = supportRepository.findByDelivered(false);
        List<Support> supportCalls = new ArrayList<>();

        if(iterable != null){
            for(Object o : iterable) {
                Support support = (Support)o;
                supportCalls.add(support);
            }

            return supportCalls;
        }
        else {
            return null;
        }
    }

}
