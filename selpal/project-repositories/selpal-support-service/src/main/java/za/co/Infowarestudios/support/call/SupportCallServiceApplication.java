package za.co.Infowarestudios.support.call;

import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.ResourceUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.mail.MessagingException;
import java.io.FileNotFoundException;

@SpringBootApplication
@EnableDiscoveryClient
@EnableScheduling
public class SupportCallServiceApplication {

	@Bean
	@Profile("dev")
	public FilterRegistrationBean corsFilterdev() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("HEAD");
		config.addAllowedMethod("POST");
		source.registerCorsConfiguration("/**", config);
		final FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
		bean.setOrder(0);
		return bean;
	}

	@Bean
	@Profile("QA")
	public FilterRegistrationBean corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("HEAD");
		config.addAllowedMethod("POST");
		source.registerCorsConfiguration("/**", config);
		final FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
		bean.setOrder(0);
		return bean;
	}

	@Bean
	@Profile("prod")
	public FilterRegistrationBean corsFilterProd() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("HEAD");
		config.addAllowedMethod("POST");
		source.registerCorsConfiguration("/**", config);
		final FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
		bean.setOrder(0);
		return bean;
	}

	@Bean
	@Profile("QA")
	public EmbeddedServletContainerCustomizer containerCustomizer(
			@Value("${keystore.file}") String keystoreFile,
			@Value("${keystore.password}") String keystorePassword,
			@Value("${keystore.type}") String keystoreType,
			@Value("${keystore.alias}") String keystoreAlias) throws FileNotFoundException
	{
		final String absoluteKeystoreFile = ResourceUtils.getFile(keystoreFile).getAbsolutePath();
		return factory -> {
			TomcatEmbeddedServletContainerFactory containerFactory = (TomcatEmbeddedServletContainerFactory) factory;
			containerFactory.addConnectorCustomizers((TomcatConnectorCustomizer) (Connector connector) -> {
				connector.setSecure(true);
				connector.setScheme("https");
				connector.setAttribute("keystoreFile", absoluteKeystoreFile);
				connector.setAttribute("keystorePass", keystorePassword);
				connector.setAttribute("keystoreType", keystoreType);
				connector.setAttribute("keyAlias", keystoreAlias);
				connector.setAttribute("clientAuth", "false");
				connector.setAttribute("sslProtocol", "TLS");
				connector.setAttribute("SSLEnabled", true);
			});
		};
	}

	@Bean
	@Profile("prod")
	public EmbeddedServletContainerCustomizer containerCustomizerProd(
			@Value("${keystore.file}") String keystoreFile,
			@Value("${keystore.password}") String keystorePassword,
			@Value("${keystore.type}") String keystoreType,
			@Value("${keystore.alias}") String keystoreAlias) throws FileNotFoundException
	{
		final String absoluteKeystoreFile = ResourceUtils.getFile(keystoreFile).getAbsolutePath();
		return factory -> {
			TomcatEmbeddedServletContainerFactory containerFactory = (TomcatEmbeddedServletContainerFactory) factory;
			containerFactory.addConnectorCustomizers((TomcatConnectorCustomizer) (Connector connector) -> {
				connector.setSecure(true);
				connector.setScheme("https");
				connector.setAttribute("keystoreFile", absoluteKeystoreFile);
				connector.setAttribute("keystorePass", keystorePassword);
				connector.setAttribute("keystoreType", keystoreType);
				connector.setAttribute("keyAlias", keystoreAlias);
				connector.setAttribute("clientAuth", "false");
				connector.setAttribute("sslProtocol", "TLS");
				connector.setAttribute("SSLEnabled", true);
			});
		};
	}

	public static void main(String[] args) throws MessagingException {
		SpringApplication.run(SupportCallServiceApplication.class, args);
	}
}
