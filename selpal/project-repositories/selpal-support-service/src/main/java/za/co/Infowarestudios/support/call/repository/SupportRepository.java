package za.co.Infowarestudios.support.call.repository;

import org.springframework.data.repository.CrudRepository;
import za.co.Infowarestudios.support.call.entity.Support;

import java.util.List;

/**
 * Created by seanmatabire on 2016/04/12.
 */

public interface SupportRepository extends CrudRepository<Support,Long> {


    List<Support> findByDelivered(boolean delivered);
}
