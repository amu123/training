package za.co.Infowarestudios.support.call.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.joda.time.DateTime;

import javax.persistence.*;

/**
 * Created by seanmatabire on 2016/04/12.
 */
@Entity
@Table(name="support")
public class Support {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "support_Id")
    private long Id;

    private String email;

    private String subject;

    private String body;

    private boolean delivered;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "CAT")
    private DateTime date;

    public Support(String subject, String body, boolean delivered, DateTime date) {
        this.subject = subject;
        this.body = body;
        this.delivered = delivered;
        this.date = date;
    }

    public Support(String email, String subject, String body, boolean delivered) {
        this.email = email;
        this.subject = subject;
        this.body = body;
        this.delivered = delivered;
    }

    public Support(String subject, String body) {
        this.subject = subject;
        this.body = body;
    }

    public Support(long id, String subject, String body, boolean delivered, DateTime date) {
        Id = id;
        this.subject = subject;
        this.body = body;
        this.delivered = delivered;
        this.date = date;
    }

    public Support(long id, String subject, String body) {
        Id = id;
        this.subject = subject;
        this.body = body;
    }

    public Support() {
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }
}
